#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log2 "Update chart dependencies"
helm repo add bitnami https://charts.bitnami.com/bitnami
helm dependency build $CHART_DIR --skip-refresh

log2 "Lint chart template files"
for yaml in .gitlab/ci/kube/values/*.yaml; do
  log2 "Validating $CHART_DIR with $yaml" "-"

  helm lint $CHART_DIR --strict --values $yaml
done

#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

if [[ ! -f "success" ]]; then
  log2 "Events of namespace ${NAMESPACE}"
  kubectl get events --output wide --namespace ${NAMESPACE}

  for pod in $(kubectl get pods --no-headers --namespace ${NAMESPACE} --output jsonpath={.items[*].metadata.name}); do
    log2 "Description of pod ${pod}"
    kubectl describe pod ${pod} --namespace ${NAMESPACE}

    for container in $(kubectl get pods ${pod} --no-headers --namespace ${NAMESPACE} --output jsonpath={.spec.initContainers[*].name}); do
      log2 "Logs of container ${pod}/${container}" "-"
      kubectl logs ${pod} --namespace ${NAMESPACE} --container ${container}
    done

    for container in $(kubectl get pods ${pod} --no-headers --namespace ${NAMESPACE} --output jsonpath={.spec.containers[*].name}); do
      log2 "Logs of container ${pod}/${container}" "-"
      kubectl logs ${pod} --namespace ${NAMESPACE} --container ${container}
    done
  done
fi
